<?php

namespace Database\Seeders;

use App\Models\OrderStatus;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_statuses');
        collect([
            'Placed',
            'Pending',
            'Shipped',
            'In-Transit',
            'Fulfilled',
            'Cancelled-Seller',
            'Cancelled-Buyer'
        ])
        ->foreach(function($status)
        {
            OrderStatus::factory()->create(['title' => $status]);
        });
    }
}
