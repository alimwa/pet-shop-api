<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        if (Role::where('role', 'admin')->get()->count() == 0)
            Role::create(['role' => 'admin', 'role_display_name' => 'Admin']);

        if (Role::where('role', 'user')->get()->count() == 0)
            Role::create(['role' => 'user', 'role_display_name' => 'User']);

        if (Role::where('role', 'marketer')->get()->count() == 0)
            Role::create(['role' => 'marketer', 'role_display_name' => 'Marketer']);
    }
}
