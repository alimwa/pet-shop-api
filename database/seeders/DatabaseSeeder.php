<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesSeeder::class);

        $this->call(UserSeeder::class);

        $this->call(PromotionsSeeder::class);

        $this->call(PostsSeeder::class);

        $this->call(BrandsSeeder::class);

        $this->call(CategoriesSeeder::class);

        $this->call(OrderStatusesSeeder::class);
    }
}
