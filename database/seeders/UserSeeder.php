<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Database\Factories\UserFactory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(User::where('email', 'admin@buckhill.co.uk')->get()->count() == 0)
            User::factory()->create([
                'email' => 'admin@buckhill.co.uk',
                'role_id' => Role::where('role', 'admin')->get()->first()->id
            ]);

        if(User::where('email', 'user@buckhill.co.uk')->get()->count() == 0)
            User::factory()->create([
                'email' => 'user@buckhill.co.uk',
                'role_id' => Role::where('role', 'user')->get()->first()->id
            ]);
    }
}
