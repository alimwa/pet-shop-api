<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            'Food',
            'Treats',
            'Accessories',
            'Cages',
            'Carriers',
            'Aquariums',
            'Toys',
            'Grooming',
            'Medicine'
        ])
        ->foreach(function($category)
        {
            Category::factory()->create([
                'title' => $category,
                'slug' => Str::of($category)->slug()
            ]);
        });
    }
}
