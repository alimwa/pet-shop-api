<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;


class FileFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $image = $this->faker->image(storage_path('app/pet-shop'),1024,680, null, false);
        return [
            'name' => Str::before($image, "."),
            'path' => $image,
            'size' => Storage::disk('local')->size('pet-shop/'.$image),
            'type' => Storage::disk('local')->mimeType('pet-shop/'.$image)
        ];
    }
}
