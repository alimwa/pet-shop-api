<?php

namespace Database\Factories;

use App\Models\File;
use App\Traits\HasContent;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;


class PostFactory extends Factory
{
    use HasContent;
    
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $post = $this->getData();
        return [
            'title' => Str::title($post->title),
            'slug' => Str::of($post->title)->slug('-'),
            'content' => $post->content,
            'metadata' => json_encode($post->metadata)
        ];
    }

    public function getData()
    {
        $post = $this->getRandomPostData();
        $post->metadata = [
            'author' => $this->faker->name(),
            'image' => File::factory()->create()->uuid
        ];
        return $post;
    }
}
