<?php

namespace Database\Factories;

use App\Models\File;
use App\Traits\HasContent;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class PromotionFactory extends Factory
{
    use HasContent;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $promotion = $this->getData();
        return [
            'title' => Str::title($promotion->title),
            'content' => $promotion->content,
            'metadata' => json_encode($promotion)
        ];
    }

    public function getData()
    {
        $promotion = $this->getRandomPostData();
        $start = Carbon::now()->addDays(rand(0, 365));
        $end = $start->addDays(rand(0, 365)); 
        $promotion->metadata = [
            'valid_from' => $start,
            'valid_to' => $end,
            'image' => File::factory()->create()->uuid
        ];
        return $promotion;
    }
}
