## Buckhill Pet Shop API

Laravel API for a Pet Shop

# Remarks

[Video 1](https://www.loom.com/share/b2c3bb5a08c54ff2a954976d00a57865)
[Video 2](https://www.loom.com/share/6728b2d80a44420a9e45a4d2348bdc46)
[Video 3](https://www.loom.com/share/acde452a0d8c459a80b27592b983ca7e)

# Installation
Clone repository from BitBucket

`git clone https://alimwa@bitbucket.org/alimwa/pet-shop-api.git`

Install Laravel Packages

`composer install`

Copy Environment File

`cp .env.example .env`

Create Database and Add Credentials for Access to it int the Environment File

```DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=pet_shop_db
DB_USERNAME=root
DB_PASSWORD=
```

Create Sym Link for Storage

`php artisan storage:link`

Make sure the APP_URL is configured to the right value for the uploads to work

Run Migrations

`php artisan migrate`

Run Seeds

`php artisan db:seed`

Run Web Server

`php artisan serve`

# Completed Features
- Admin Endpoints
- User Endpoints
- Main Page Endpoints
- File Uploads Endpoints
- Categories Endpoints
- Brands Endpoints
- API Documentation

# Partialy Implemented Features
- Listings pagination
- User Orders
- Lcobucci Token Generation and setup **Documentation was a little unclear so I did as much as I could cover then fell back on Laravel Sanctum. Might want to look into contributing better docs and workflows on it**


# Todo Features
- Product Endpoints
- Order Endpoints
- Payment Endpoints
- Password Recovery Endpoints