<?php

use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Signer\Rsa\Sha256;

return [

    /*
    |--------------------------------------------------------------------------
    | LCobbuci Configuration Paths
    |--------------------------------------------------------------------------
    |
    | This part initialises a few things for LCobbuci Configuration
    |
    */

    'paths' => [
        'lcobbuci_base' => base_path('lcobucci_keys/'),
        'private' => 'private.pem',
        'public' => 'public.pem'
    ],

    // 'configuration' => env('APP_ENV') == 'testing' ? null : Configuration::forAsymmetricSigner(
    //     new Sha256(),   
    //     InMemory::file(base_path('lcobucci_keys/private.pem')),
    //     InMemory::file(base_path('lcobucci_keys/private.pem'))
    // )

];
