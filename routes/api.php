<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\UserAPIController;
use App\Http\Controllers\API\AdminAPIController;
use App\Http\Controllers\API\FileAPIController;
use App\Http\Controllers\API\MainPageController;
use App\Http\Controllers\API\CategoryAPIController;
use App\Http\Controllers\API\BrandAPIController;

/*
use App\Http\Controllers\API\CategoryAPIController;
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('user')->as('api.user.')->group(function () 
{
    Route::post('/create', [UserAPIController::class, 'save'])->name('save');
    Route::post('/login', [UserAPIController::class, 'login'])->name('login');
    Route::match(['get', 'head'], '/logout', [UserAPIController::class, 'logout'])->name('logout')->middleware('auth:sanctum');
    Route::match(['get', 'head'], '', [UserAPIController::class, 'user'])->name('user')->middleware('auth:sanctum');
    Route::match(['get', 'head'], '/orders', [UserAPIController::class, 'orders'])->name('orders')->middleware('auth:sanctum');
    Route::put('/edit', [UserAPIController::class, 'edit'])->name('edit')->middleware('auth:sanctum');
    Route::delete('', [UserAPIController::class, 'delete_account'])->name('delete')->middleware('auth:sanctum');
});


Route::prefix('admin')->as('api.admin.')->group(function () 
{
    Route::post('/create', [AdminAPIController::class, 'save'])->name('save');
    Route::post('/login', [AdminAPIController::class, 'login'])->name('login');
    Route::match(['get', 'head'], '/logout', [AdminAPIController::class, 'logout'])->name('logout')->middleware(['auth:sanctum', 'is_admin']);
    Route::match(['get', 'head'], '/user-listing', [AdminAPIController::class, 'user_listing'])->name('user-listing')->middleware(['auth:sanctum', 'is_admin']);
    Route::delete('/user-delete/{uuid}', [AdminAPIController::class, 'destroy_user_account'])->name('user_delete')->middleware(['auth:sanctum', 'is_admin']);
});


Route::prefix('file')->as('api.file.')->group(function() 
{
    Route::post('/upload', [FileAPIController::class, 'upload'])->name('upload')->middleware('auth:sanctum');
    Route::match(['get', 'head'], '/{uuid}', [FileAPIController::class, 'download'])->name('download');
});


Route::prefix('main')->as('api.main')->group(function() 
{
    Route::match(['get', 'head'], '/promotions ', [MainPageController::class, 'promotions'])->name('promotions ');
    Route::prefix('blog')->as('blog.')->group(function() 
    {
        Route::match(['get', 'head'], '', [MainPageController::class, 'blog'])->name('all_blog_posts');
        Route::match(['get', 'head'], '/{uuid}', [MainPageController::class, 'single'])->name('single_blog_post');
    });
});


Route::match(['get', 'head'], 'brands', [BrandAPIController::class, 'index'])->name('api.brands.index');
    
Route::prefix('brand')->as('api.brands.')->group(function()
{
    Route::post('/create', [BrandAPIController::class, 'store'])->name('store')->middleware('auth:sanctum');
    Route::match(['get', 'head'], '/{uuid}', [BrandAPIController::class, 'show'])->name('show');
    Route::match(['put', 'patch'], '/{uuid}', [BrandAPIController::class, 'update'])->name('update')->middleware('auth:sanctum');
    Route::delete('/{uuid}', [BrandAPIController::class, 'destroy'])->name('destroy')->middleware('auth:sanctum');
});


Route::match(['get', 'head'], 'categories', [CategoryAPIController::class, 'index'])->name('api.categories.index');
    
Route::prefix('category')->as('api.category.')->group(function()
{
    Route::post('/create', [CategoryAPIController::class, 'store'])->name('store')->middleware('auth:sanctum');
    Route::match(['get', 'head'], '/{uuid}', [CategoryAPIController::class, 'show'])->name('show');
    Route::match(['put', 'patch'], '/{uuid}', [CategoryAPIController::class, 'update'])->name('update')->middleware('auth:sanctum');
    Route::delete('/{uuid}', [CategoryAPIController::class, 'destroy'])->name('destroy')->middleware('auth:sanctum');
});


Route::match(['get', 'head'], 'order-status', [OrderStatusAPIController::class, 'index'])->name('api.order_statuses.index');
    
Route::prefix('order-status')->as('api.order_status.')->group(function()
{
    Route::post('/create', [OrderStatusAPIController::class, 'store'])->name('store')->middleware('auth:sanctum');
    Route::match(['get', 'head'], '/{uuid}', [OrderStatusAPIController::class, 'show'])->name('show');
    Route::match(['put', 'patch'], '/{uuid}', [OrderStatusAPIController::class, 'update'])->name('update')->middleware('auth:sanctum');
    Route::delete('/{uuid}', [OrderStatusAPIController::class, 'destroy'])->name('destroy')->middleware('auth:sanctum');
});
