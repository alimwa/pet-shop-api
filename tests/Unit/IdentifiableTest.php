<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class IdentifiableTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A test to check if an identifiable model creates records with a valid UUID
     *
     * @return void
     */
    public function test_creates_user_with_valid_uuid()
    {
        $user = User::factory()->create();

        $this->assertTrue($user->hasValidUUID());
    }
}
