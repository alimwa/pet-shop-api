<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\HasUser;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;

class UserAthenticationTest extends TestCase
{
    use RefreshDatabase, HasUser;

    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan("db:seed --class=RolesSeeder");
        $this->artisan("db:seed --class=UserSeeder");
    }

    /**
     * Test user login with valid credentials
     *
     * @return void
     */
    public function test_user_login_with_valid_credentials()
    {
        $userCredentials = $this->getUserCredentials();
        
        $response = $this->post('/api/v1/user/login', $userCredentials);        
        
        $response->assertStatus(200);
    }

    /**
     * Test admin login with valid credentials
     *
     * @return void
     */
    public function test_admin_login_with_valid_credentials()
    {
        $userCredentials = $this->getAdminCredentials();
        
        $response = $this->post('/api/v1/admin/login', $userCredentials);       
        
        $response->assertStatus(200);
    }

    /**
     * Test admin login fail with invalid credentials
     *
     * @return void
     */
    public function admin_login_fail_with_normal_user_credentials()
    {
        $userCredentials = $this->getUserCredentials();
        
        $response = $this->post('/api/v1/admin/login', $userCredentials);       
        
        $response->assertStatus(403);
    }


    /**
     * Test user login fail with invalid credentials
     *
     * @return void
     */
    public function test_user_login_fail_with_invalid_credentials()
    {
        $userCredentials = $this->getUserCredentials();

        $userCredentials['password'] = $userCredentials['password'].'13';
        
        $response = $this->post('/api/v1/user/login', $userCredentials);        
        
        $response->assertStatus(401);
    }

    /**
     * Test admin login fail with invalid credentials
     *
     * @return void
     */
    public function test_admin_fail_login_with_invalid_credentials()
    {
        $userCredentials = $this->getAdminCredentials();

        $userCredentials['password'] = $userCredentials['password'].'13';
        
        $response = $this->post('/api/v1/admin/login', $userCredentials);       
        
        $response->assertStatus(401);
    }


    /**
     * Test user logout without token
     *
     * @return void
     */
    public function test_user_logout_without_token()
    {

        $logoutResponse = $this->get('/api/v1/user/logout');

        $logoutResponse->assertStatus(401);
    }

    /**
     * Test admin logout without token
     *
     * @return void
     */
    public function test_admin_logout_without_token()
    {

        $logoutResponse = $this->get('/api/v1/admin/logout');

        $logoutResponse->assertStatus(401);
    }

    /**
     * Test user logout with token
     *
     * @return void
     */
    public function test_user_logout_with_token()
    {
        Sanctum::actingAs(
            User::find(2)
        );

        $logoutResponse = $this->get('/api/v1/user/logout');

        $logoutResponse->assertStatus(200);
    }

    /**
     * Test admin logout with token
     *
     * @return void
     */
    public function test_admin_logout_with_token()
    {
        Sanctum::actingAs(
            User::find(1)
        );

        $logoutResponse = $this->get('/api/v1/admin/logout');

        $logoutResponse->assertStatus(200);
    }

}
