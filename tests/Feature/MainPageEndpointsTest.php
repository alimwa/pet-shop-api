<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\Post;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class MainPageEndpointsTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan("storage:link");
        $this->artisan("db:seed --class=PostsSeeder");
        $this->artisan("db:seed --class=PromotionsSeeder");
    }

    public function tearDown(): void
    {
        parent::tearDown();
        //File::delete(File::glob(storage_path('app/pet-shop/*.png')));
    }


    /**
     * All promotions can be retrieved.
     *
     * @return void
     */
    public function test_all_promotions_can_be_retrieved()
    {
        $response = $this->get('/api/v1/main/promotions');

        $response->assertStatus(200);
    }

    /**
     * All posts can be retrieved.
     *
     * @return void
     */
    public function test_all_posts_can_be_retrieved()
    {
        $response = $this->get('/api/v1/main/blog');

        $response->assertStatus(200);
    }


    /**
     * A single post can be retrieved with a valid uuid.
     *
     * @return void
     */
    public function test_single_post_can_be_retrieved_with_a_valid_uuid()
    {
        $post = Post::all()->first();

        $response = $this->get('/api/v1/main/blog/'.$post->uuid);

        $response->assertStatus(200);
    }


    /**
     * A single post cannot be retrieved with an invalid uuid.
     *
     * @return void
     */
    public function test_single_post_cannot_be_retrieved_with_an_invalid_uuid()
    {
        $post = Post::all()->first();

        $response = $this->get('/api/v1/main/blog/8'.$post->uuid); // the 8 invalidates the uuid supplied

        $response->assertStatus(404);
    }
}
