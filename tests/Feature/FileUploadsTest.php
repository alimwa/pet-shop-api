<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\File;
use Laravel\Sanctum\Sanctum;
use Tests\HasUser;

class FileUploadsTest extends TestCase
{
    use RefreshDatabase;

    use HasUser;


    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan("storage:link");
        $this->artisan("db:seed --class=RolesSeeder");
        $this->artisan("db:seed --class=UserSeeder");
    }

    public function tearDown(): void
    {
        parent::tearDown();
        //File::delete(File::glob(storage_path('app/pet-shop/*.jpg')));
    }


    /**
     * Files can be uploaded.
     *
     * @return void
     */
    public function test_file_can_be_uploaded()
    {
        Storage::fake('local');

        Sanctum::actingAs(
            User::find(2)
        );

        $file = UploadedFile::fake()->image('image.png');

        $response = $this->postJson('/api/v1/file/upload', ['file' => $file]);

        $response->assertStatus(200);
    }

    /**
     * Files upload fails for no file or wrong name for file in form data.
     *
     * @return void
     */
    public function test_file_upload_fails_for_invalid_upload_data()
    {
        Storage::fake('local');

        Sanctum::actingAs(
            User::find(2)
        );

        $file = UploadedFile::fake()->image('image.png');

        $response = $this->postJson('/api/v1/file/upload', ['image' => $file]); // image in place of file - incorrect reference

        $response->assertStatus(422);
    }

    /**
     * Files upload fails for no file that is not an image i.e wrong mime-type.
     *
     * @return void
     */
    public function test_file_upload_fails_for_non_image_file()
    {
        Storage::fake('local');

        // non image file i.e trying to upload a pdf in place of an image
        $file = UploadedFile::fake()->create(
            'document.pdf', 250, 'application/pdf'
        );

        Sanctum::actingAs(
            User::find(2)
        );

        $response = $this->postJson('/api/v1/file/upload', ['file' => $file]); // image in place of file - incorrect reference

        $response->assertStatus(422);
    }

    /**
     * Files can be retrieved by uuid.
     *
     * @return void
     */
    public function test_file_can_be_retreived_by_uuid()
    {
        Storage::fake('local');

        Sanctum::actingAs(
            User::find(2)
        );

        $file = UploadedFile::fake()->image('image.png');

        $response = $this->postJson('/api/v1/file/upload', ['file' => $file]);

        $response->assertStatus(200);

        $uuid = json_decode($response->getContent(), true)['data']['uuid'];

        $fileResponse = $this->getJson('api/v1/file/'.$uuid);

        $fileResponse->assertDownload();

        $fileResponse = $this->getJson('api/v1/file/5'.$uuid); // invalid uuid

        $fileResponse->assertStatus(404);
    }
}
