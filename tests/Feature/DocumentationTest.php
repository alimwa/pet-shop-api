<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DocumentationTest extends TestCase
{

    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('vendor:publish --provider="Appointer\Swaggervel\SwaggervelServiceProvider"');
    }

    /**
     * A basic check if the api has a documentation
     *
     * @return void
     */
    public function test_api_has_documentation()
    {
        $response = $this->get('/api/docs');

        $response->assertStatus(200);
    }

     /**
     * A basic check if the api has a JSON for its documentation
     *
     * @return void
     */
    public function test_api_has_json_for_its_documentation()
    {
        $response = $this->get('/docs');

        $response->assertStatus(200)
                ->assertHeader('Content-Type', 'application/json');
    }
}
