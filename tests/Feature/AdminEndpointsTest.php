<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Support\Str;

class AdminEndpointsTest extends TestCase
{   
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan("db:seed --class=RolesSeeder");
        $this->artisan("db:seed --class=UserSeeder");
    }

    private function getUserRole()
    {
        $normalUserRole = Role::where('role', 'user')->get()->first();
        return ['role_id' => $normalUserRole->id];
    }

    /**
     * An admin can get users who are not admins.
     *
     * @return void
     */
    public function test_admin_can_view_user_listing()
    {
        Sanctum::actingAs(
            User::find(1)
        );

        $responseUserListing = $this->get('/api/v1/admin/user-listing');

        $responseUserListing->assertStatus(200);

        $data = json_decode($responseUserListing->getContent(), true)['data'];

        $this->assertCount(1, $data);
    }


     /**
     * A user cannot get users who are not admin.
     *
     * @return void
     */
    public function test_normal_user_cannot_view_user_listing()
    {

        Sanctum::actingAs(
            User::find(2)
        );

        $responseUserListing = $this->get('/api/v1/admin/user-listing');

        $responseUserListing->assertStatus(403);
    }

    /**
     * An admin can delete user account.
     *
     * @return void
     */
    public function test_admin_can_delete_user_account()
    {
        Sanctum::actingAs(
            User::find(1)
        );

        $user = User::factory()->create($this->getUserRole());

        $responseUserDelete = $this->delete('/api/v1/admin/user-delete/'.$user->uuid);

        $responseUserDelete->assertStatus(200);
    }


     /**
     * A user cannot delete user account.
     *
     * @return void
     */
    public function test_normal_user_cannot_delete_user_account()
    {

        Sanctum::actingAs(
            User::find(2)
        );

        $user = User::factory()->create($this->getUserRole());

        $responseUserDelete = $this->delete('/api/v1/admin/user-delete/'.$user->uuid);

        $responseUserDelete->assertStatus(403);
    }


    /**
     * Admin cannot delete user non existant account.
     *
     * @return void
     */
    public function test_admin_user_cannot_delete_non_existant_user_account()
    {

        Sanctum::actingAs(
            User::find(1)
        );

        $uuid = Str::uuid()->toString();

        $responseUserDelete = $this->delete('/api/v1/admin/user-delete/'.$uuid);

        $responseUserDelete->assertStatus(404);
    }

    /**
     * Admin cannot delete admin account.
     *
     * @return void
     */
    public function test_admin_user_cannot_delete_admin_account()
    {

        Sanctum::actingAs(
            User::find(1)
        );

        $responseUserDelete = $this->delete('/api/v1/admin/user-delete/'.User::find(1)->uuid);

        $responseUserDelete->assertStatus(403);
    }
}
