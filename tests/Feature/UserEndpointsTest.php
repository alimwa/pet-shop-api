<?php

namespace Tests\Feature;

use App\Repositories\UserRepository;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\HasUser;

class UserEndpointsTest extends TestCase
{
    use RefreshDatabase, HasUser;

    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan("db:seed --class=RolesSeeder");
        $this->artisan("db:seed --class=UserSeeder");
    }

    public function getDetailsForUpdate()
    {
        return [
            'first_name' => 'Lee',
            'last_name' => 'Muddy',
            'phone_number' => '0813958732',
            'address' => '123 Happy Road, Happy Street, Happy City, Happyland'
        ];
    }

    /**
     * A logged in user can get their details
     *
     * @return void
     */
    public function test_logged_in_user_can_get_details()
    {
        $token = $this->loginUser();

        $response = $this->withToken($token)->getJson('/api/v1/user');

        $response->assertStatus(200);
    }

    /**
     * An unauthenticated user cannot get details
     *
     * @return void
     */
    public function test_guest_user_cannot_get_details()
    {

        $response = $this->getJson('/api/v1/user');

        $response->assertStatus(401);
    }

    /**
     * A logged in user can update their details
     *
     * @return void
     */
    public function test_logged_in_user_can_update_details()
    {
        $token = $this->loginUser();

        $response = $this->withToken($token)->putJson('/api/v1/user/edit', $this->getDetailsForUpdate());

        $response->assertStatus(200);
    }

    /**
     * An unauthenticated user cannot update their details
     *
     * @return void
     */
    public function test_guest_user_cannot_update_details()
    {

        $response = $this->putJson('/api/v1/user/edit', $this->getDetailsForUpdate());

        $response->assertStatus(401);
    }


    /**
     * A logged in user can get their orders
     *
     * @return void
     */
    public function test_logged_in_user_can_get_their_orders()
    {
        $token = $this->loginUser();

        $response = $this->withToken($token)->getJson('/api/v1/user/orders');

        $response->assertStatus(200);
    }

    /**
     * An unauthenticated user cannot get their orders
     *
     * @return void
     */
    public function test_guest_user_cannot_get_their_orders()
    {

        $response = $this->getJson('/api/v1/user/orders');

        $response->assertStatus(401);
    }


    /**
     * A logged in user can delete their account
     *
     * @return void
     */
    public function test_logged_in_user_can_delete_account()
    {
        $token = $this->loginUser();

        $response = $this->withToken($token)->deleteJson('/api/v1/user');

        $response->assertStatus(200);

        $response = $this->postJson('/api/v1/user/login', $this->getUserCredentials());

        // cannot login again after deleting their account
        $response->assertStatus(401);
    }

    /**
     * An unauthenticated user cannot delete account
     *
     * @return void
     */
    public function test_guest_user_cannot_delete_account()
    {

        $response = $this->deleteJson('/api/v1/user');

        $response->assertStatus(401);
    }
}
