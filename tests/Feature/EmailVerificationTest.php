<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\HasUserData;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;

class EmailVerificationTest extends TestCase
{

    use HasUserData, RefreshDatabase;

    /**
     * A test to check if verification email is sent on registration of normal user.
     *
     * @return void
     */
    public function test_sends_out_verification_on_user_registration()
    {
        Event::fake();

        $userDetails = $this->getUserDetails();
        
        $this->postJson('/api/v1/user/create', $userDetails);
        
        Event::assertNotDispatched(Registered::class);
    }

    

    /**
     * A test to check if verification email is sent on registration of admin user.
     *
     * @return void
     */
    public function test_sends_out_verification_on_admin_registration()
    {
        Event::fake();

        $userDetails = $this->getUserDetails();
        
        $this->postJson('/api/v1/admin/create', $userDetails);
        
        Event::assertNotDispatched(Registered::class);
    }
    
}
