<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\HasUserData;

class UserRegistrationTest extends TestCase
{
    use HasUserData, RefreshDatabase;    

    /**
     * A test for the registration of a normal user.
     *
     * @return void
     */
    public function test_registration_of_simple_user()
    {
        $userDetails = $this->getUserDetails();
        
        $response = $this->postJson('/api/v1/user/create', $userDetails);
        
        $response->assertStatus(200)
                ->assertJsonPath('data.email', $userDetails['email'])
                ->assertJsonPath('data.first_name', $userDetails['first_name'])
                ->assertJsonPath('data.last_name', $userDetails['last_name']);
    }


    /**
     * A test for the registration of an admin user.
     *
     * @return void
     */
    public function test_registration_of_an_admin_user()
    {
        $userDetails = $this->getUserDetails();
        
        $response = $this->postJson('/api/v1/admin/create', $userDetails);
        
        $response->assertStatus(200)
                ->assertJsonPath('data.email', $userDetails['email'])
                ->assertJsonPath('data.first_name', $userDetails['first_name'])
                ->assertJsonPath('data.last_name', $userDetails['last_name']);
    }


    /**
     * A test for the failure of normal user registration with invalid details.
     *
     * @return void
     */
    public function test_failure_of_registration_of_simple_user_with_invalid_details()
    {
        foreach($this->getInvalidDataMethodNames() as $method)
        {
            $userDetails = $this->{$method}();
        
            $response = $this->postJson('/api/v1/user/create', $userDetails);

            $response->assertStatus(422);
        }
        
    }


    /**
     * A test for the failure of normal user registration with invalid details.
     *
     * @return void
     */
    public function test_failure_registration_of_an_admin_user_with_invalid_details()
    {
        foreach($this->getInvalidDataMethodNames() as $method)
        {
            $userDetails = $this->{$method}();
        
            $response = $this->postJson('/api/v1/admin/create', $userDetails);

            $response->assertStatus(422);
        }
    }
}
