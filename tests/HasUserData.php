<?php

namespace Tests;

use Illuminate\Support\Arr;
use Illuminate\Foundation\Testing\WithFaker;


trait HasUserData
{
    use WithFaker;


    /**
     * Generates fake details for registration.
     *
     * @return array
     */
    public function getUserDetails()
    {
        $faker = $this->faker();
        
        return [
            'first_name' => $faker->firstName(),
            'last_name' => $faker->lastName(),
            'email' => $faker->unique()->safeEmail(),
            'password' => 'password', 
            'password_confirmation' => 'password'        
        ];
    }

    public function getUserWithMissMatchingPasswordConfirmation()
    {
        $userDetails = $this->getUserDetails();
        $userDetails['password'] = $userDetails['password'].'13';
        return $userDetails;
    }

    public function getUserWithShortPassword()
    {
        $userDetails = $this->getUserDetails();
        $userDetails['password'] = 'pass';
        $userDetails['password_confirmation'] = 'pass';
        return $userDetails;
    }

    public function getUserWithNoFirstName()
    {
        $userDetails = $this->getUserDetails();
        
        return Arr::except($userDetails, ['first_name']);
    }

    public function getUserWithNoLastName()
    {
        $userDetails = $this->getUserDetails();
        
        return Arr::except($userDetails, ['last_name']);
    }

    public function getUserWithNoEmail()
    {
        $userDetails = $this->getUserDetails();
        
        return Arr::except($userDetails, ['email']);
    }

    public function getUserWithNoPassword()
    {
        $userDetails = $this->getUserDetails();
        
        return Arr::except($userDetails, ['password']);
    }

    public function getUserWithInvalidEmail()
    {
        $userDetails = $this->getUserDetails();
        $userDetails['email'] = $userDetails['first_name'];
        return $userDetails;
    }

    public function getInvalidDataMethodNames()
    {
        return [
            'getUserWithMissMatchingPasswordConfirmation',
            'getUserWithShortPassword',
            'getUserWithNoFirstName',
            'getUserWithNoLastName',
            'getUserWithNoEmail',
            'getUserWithNoPassword',
            'getUserWithInvalidEmail'
        ];
    }
}

