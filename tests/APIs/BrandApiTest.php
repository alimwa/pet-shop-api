<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Brand;

class BrandApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_brand()
    {
        $brand = Brand::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/v1/brand/create', $brand
        );

        $this->assertApiResponse($brand);
    }

    /**
     * @test
     */
    public function test_read_brand()
    {
        $brand = Brand::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/v1/brand/'.$brand->uuid
        );

        $this->assertApiResponse($brand->toArray());
    }

    /**
     * @test
     */
    public function test_update_brand()
    {
        $brand = Brand::factory()->create();
        $editedBrand = Brand::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/v1/brand/'.$brand->uuid,
            $editedBrand
        );

        $this->assertApiResponse($editedBrand);
    }

    /**
     * @test
     */
    public function test_delete_brand()
    {
        $brand = Brand::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/v1/brand/'.$brand->uuid
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/v1/brand/'.$brand->uuid
        );

        $this->response->assertStatus(404);
    }
}
