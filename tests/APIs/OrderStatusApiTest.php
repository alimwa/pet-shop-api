<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\OrderStatus;

class OrderStatusApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_order_status()
    {
        $orderStatus = OrderStatus::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/v1/order-status/create', $orderStatus
        );

        $this->assertApiResponse($orderStatus);
    }

    /**
     * @test
     */
    public function test_read_order_status()
    {
        $orderStatus = OrderStatus::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/v1/order-status/'.$orderStatus->uuid
        );

        $this->assertApiResponse($orderStatus->toArray());
    }

    /**
     * @test
     */
    public function test_update_order_status()
    {
        $orderStatus = OrderStatus::factory()->create();
        $editedOrderStatus = OrderStatus::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/v1/order-status/'.$orderStatus->uuid,
            $editedOrderStatus
        );

        $this->assertApiResponse($editedOrderStatus);
    }

    /**
     * @test
     */
    public function test_delete_order_status()
    {
        $orderStatus = OrderStatus::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/v1/order-status/'.$orderStatus->uuid
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/v1/order-status/'.$orderStatus->uuid
        );

        $this->response->assertStatus(404);
    }
}
