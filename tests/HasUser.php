<?php

namespace Tests;


trait HasUser 
{
    public function getUserCredentials()
    {
        return ['email' => 'admin@buckhill.co.uk', 'password' => 'password'];
    }

    public function getAdminCredentials(){
        return ['email' => 'user@buckhill.co.uk', 'password' => 'password'];
    }

    public function loginUser()
    {
        return $this->login('/api/v1/user/login', $this->getUserCredentials()); 
    }

    public function loginAdmin()
    {
        return $this->login('/api/v1/admin/login', $this->getAdminCredentials()); 
    }

    public function login($endpoint, $credentials)
    {
        $response = $this->postJson($endpoint, $credentials); 
        return $this->extractToken($response);
    }

    public function extractToken($response)
    {
        return json_decode($response->getContent(), true)['data']['token'];
    }
}