<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserCreationRequest;
use App\Http\Requests\UserEditRequest;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Request;

class UserAPIController extends AppBaseController
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }


    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/user/create",
     *      summary="Create an normal user",
     *      tags={"User"},
     *      description="Create an normal user",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="User Details",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="email",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="password",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="last_name",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="first_name",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="password_confirmation",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="object",
     *                  @SWG\Schema(
     *                      type="object",
     *                      @SWG\Property(
     *                          property="token",
     *                          type="string"
     *                      )
     *                  )
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=422,
     *          description="invalid data",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )  
     * )
     */    
    public function save(UserCreationRequest $request)
    {
        // create a normal user with the repository
        $user = $this->userRepository->create($request->validated());

        // return created user
        return $this->sendResponse($user->toArray(),'User Created SuccessFully');
    }


    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/user/login",
     *      summary="Login as a normal user",
     *      tags={"User"},
     *      description="Login as a normal user",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="User credentials",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="email",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="password",
     *                  type="string"
     *              ),
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="object",
     *                  @SWG\Schema(
     *                      type="object",
     *                      @SWG\Property(
     *                          property="token",
     *                          type="string"
     *                      )
     *                  )
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=422,
     *          description="invalid data",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=401,
     *          description="invalid credentials",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     *  
     * )
     */    
    public function login(LoginRequest $request)
    {
        $credentials = $request->validated();
        $token = $this->userRepository->login($credentials);
        return $token ? $this->sendResponse(['token' => $token], 'Login SuccessFul') : $this->sendError('Invalid Credentials', 401);
    }    


    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/user/logout",
     *      summary="Logout as a normal user",
     *      tags={"User"},
     *      description="Logout as a normal user",
     *      produces={"application/json"},
     *      security={ {"api_key": {}} },
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */   
    public function logout()
    {
        $success = $this->userRepository->logout();
        return $success ? $this->sendSuccess('Logout SuccessFul'): $this->sendError('Error Logging Out', 500);
    }


    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/user",
     *      summary="Get account details of current logged in user",
     *      tags={"User"},
     *      description="Get account details of current logged in user",
     *      produces={"application/json"},
     *      security={ {"api_key": {}} },
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/User"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function user()
    {
        return $this->sendResponse(request()->user()->toArray(), 'Account details retrieved successfully');
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/user/edit",
     *      summary="Edit logged in user details",
     *      tags={"User"},
     *      description="Edit logged in user details",
     *      produces={"application/json"},
     *      security={ {"api_key": {}} },
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="User details",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="first_name",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="last_name",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="phone_number",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="address",
     *                  type="string"
     *              ),
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="object",
     *                  @SWG\Schema(
     *                      type="object",
     *                      @SWG\Property(
     *                          property="token",
     *                          type="string"
     *                      )
     *                  )
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=422,
     *          description="invalid data",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=401,
     *          description="invalid credentials",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     *  
     * )
     */      
    public function edit(UserEditRequest $request)
    {
        $updated = $this->userRepository->edit($request->validated());

        return $updated ?
         $this->sendResponse($request->user()->toArray(), 'User updated successfully') : 
         $this->sendError('Error updating user details', 500);
    }


     /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Delete(
     *      path="/user",
     *      summary="Delete user account",
     *      tags={"User"},
     *      description="Delete user account",
     *      produces={"application/json"},
     *      security={ {"api_key": {}} },
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */   
    public function delete_account()
    {
        $success = $this->userRepository->delete();
        return $success ? $this->sendSuccess('Account deleted successfully'): $this->sendError('Error deleting user account', 500);
    }


    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/user/orders",
     *      summary="Get user orders",
     *      tags={"User"},
     *      description="Get user orders",
     *      produces={"application/json"},
     *      security={ {"api_key": {}} },
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */   
    public function orders()
    {
        return $this->sendResponse($this->userRepository->orders(), 'User Orders Retrived SuccessFully');
    }
}
