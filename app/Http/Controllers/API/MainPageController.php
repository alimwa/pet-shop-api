<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Models\Post;
use App\Repositories\PostRepository;
use App\Repositories\PromotionRepository;
use Illuminate\Http\Request;

class MainPageController extends AppBaseController
{
    private $promotionRepo;
    private $postRepo;

    public function __construct(PostRepository $postRepo, PromotionRepository $promotionRepo)
    {
        $this->promotionRepo = $promotionRepo;
        $this->postRepo = $postRepo;
    }


    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/main/promotions",
     *      summary="Get a listing of the promotions",
     *      tags={"Main"},
     *      description="Get a listing of the promotions",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Promotion")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function promotions(Request $request)
    {
        $promotions = $this->promotionRepo->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($promotions->toArray(), 'Promotions retrieved successfully');
    }


    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/main/blog",
     *      summary="Get a listing of the posts",
     *      tags={"Main"},
     *      description="Get a listing of the posts",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Post")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function blog(Request $request)
    {
        $posts = $this->postRepo->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($posts->toArray(), 'Posts retrieved successfully');
    }


    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/main/blog/{uuid}",
     *      summary="Get a single blog post",
     *      tags={"Main"},
     *      description="Get a single blog post",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="uuid",
     *          description="uuid of Brand",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Post"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=404,
     *          description="invalid uuid, post not found",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function single($uuid)
    {
        $post = Post::where('uuid', $uuid)->get()->first();

        return $post ? $this->sendResponse($post->toArray(), 'Post retrieved successfully') : $this->sendError('Post not found due to invalid uuid', 404);
    }
}
