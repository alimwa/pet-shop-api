<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\FileUploadRequest;
use App\Repositories\FileRepository;
use Illuminate\Support\Facades\Storage;

class FileAPIController extends AppBaseController
{
    private $fileRepository;


    public function __construct(FileRepository $fileRepository)
    {
        $this->fileRepository = $fileRepository;
    }


    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/file/upload",
     *      summary="Upload file",
     *      tags={"File"},
     *      description="Upload file",
     *      produces={"application/json"},
     *      consumes={"multipart/form-data"},
     *      security={ {"api_key": {}} },
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="File to be uploaded",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="file",
     *                  type="file"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/File"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=422,
     *          description="invalid data",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=401,
     *          description="unauthenticated",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */    
    public function upload(FileUploadRequest $request)
    {
        $file = $this->fileRepository->save();
        
        return $file ? $this->sendResponse($file->toArray(), 'File uploaded successfully') : $this->sendError('Error uploading file', 500);
    }


    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/file/{uuid}",
     *      summary="Download image file",
     *      tags={"File"},
     *      description="Download image file",
     *      produces={"application/file"},
     *      @SWG\Parameter(
     *          name="uuid",
     *          in="path",
     *          type="string",
     *          description="Unique Identifier",
     *          required=true
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation"
     *      ),
     *      @SWG\Response(
     *          response=404,
     *          description="image not found",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */   
    public function download($uuid)
    {
        $fileRecord = $this->fileRepository->getByUUID($uuid);
        
        return $fileRecord ? Storage::download($fileRecord->path) : $this->sendError('File not found.');
    }
}
