<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOrderStatusAPIRequest;
use App\Http\Requests\API\UpdateOrderStatusAPIRequest;
use App\Models\OrderStatus;
use App\Repositories\OrderStatusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class OrderStatusController
 * @package App\Http\Controllers\API
 */

class OrderStatusAPIController extends AppBaseController
{
    /** @var  OrderStatusRepository */
    private $orderStatusRepository;

    public function __construct(OrderStatusRepository $orderStatusRepo)
    {
        $this->orderStatusRepository = $orderStatusRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/order-statuses",
     *      summary="Get a listing of the Order Statuses.",
     *      tags={"OrderStatus"},
     *      description="Get all Order Statuses",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/OrderStatus")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $orderStatuses = $this->orderStatusRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($orderStatuses->toArray(), 'Order Statuses retrieved successfully');
    }

    /**
     * @param CreateOrderStatusAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/order-status/create",
     *      summary="Store a newly created OrderStatus in storage",
     *      tags={"OrderStatus"},
     *      description="Store OrderStatus",
     *      produces={"application/json"},
     *      security={ {"api_key": {}} },
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="OrderStatus that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/OrderStatus")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/OrderStatus"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateOrderStatusAPIRequest $request)
    {
        $input = $request->all();

        $orderStatus = $this->orderStatusRepository->create($input);

        return $this->sendResponse($orderStatus->toArray(), 'Order Status saved successfully');
    }

    /**
     * @param string $uuid
     * @return Response
     *
     * @SWG\Get(
     *      path="/order-status/{uuid}",
     *      summary="Display the specified OrderStatus",
     *      tags={"OrderStatus"},
     *      description="Get OrderStatus",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="uuid",
     *          description="uuid of OrderStatus",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/OrderStatus"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($uuid)
    {
        /** @var OrderStatus $orderStatus */
        $orderStatus = $this->orderStatusRepository->findByUUID($uuid);

        if (empty($orderStatus)) {
            return $this->sendError('Order Status not found');
        }

        return $this->sendResponse($orderStatus->toArray(), 'Order Status retrieved successfully');
    }

    /**
     * @param string $uuid
     * @param UpdateOrderStatusAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/order-status/{uuid}",
     *      summary="Update the specified OrderStatus in storage",
     *      tags={"OrderStatus"},
     *      description="Update OrderStatus",
     *      produces={"application/json"},
     *      security={ {"api_key": {}} },
     *      @SWG\Parameter(
     *          name="uuid",
     *          description="uuid of OrderStatus",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="OrderStatus that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/OrderStatus")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/OrderStatus"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($uuid, UpdateOrderStatusAPIRequest $request)
    {
        $input = $request->all();

        /** @var OrderStatus $orderStatus */
        $orderStatus = $this->orderStatusRepository->findByUUID($uuid);

        if (empty($orderStatus)) {
            return $this->sendError('Order Status not found');
        }

        $orderStatus = $this->orderStatusRepository->update($input, $orderStatus->id);

        return $this->sendResponse($orderStatus->toArray(), 'OrderStatus updated successfully');
    }

    /**
     * @param string $uuid
     * @return Response
     *
     * @SWG\Delete(
     *      path="/order-status/{uuid}",
     *      summary="Remove the specified OrderStatus from storage",
     *      tags={"OrderStatus"},
     *      description="Delete OrderStatus",
     *      produces={"application/json"},
     *      security={ {"api_key": {}} },
     *      @SWG\Parameter(
     *          name="uuid",
     *          description="uuid of OrderStatus",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($uuid)
    {
        /** @var OrderStatus $orderStatus */
        $orderStatus = $this->orderStatusRepository->findByUUID($uuid);

        if (empty($orderStatus)) {
            return $this->sendError('Order Status not found');
        }

        $orderStatus->delete();

        return $this->sendSuccess('Order Status deleted successfully');
    }
}
