<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserCreationRequest;
use App\Repositories\UserAccountRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class AdminAPIController extends AppBaseController
{
    private $userRepository;
    private $userAccountRepository;

    public function __construct(UserRepository $userRepository, UserAccountRepository $userAccountRepository)
    {
        $this->userRepository = $userRepository;
        $this->userAccountRepository = $userAccountRepository;
    }


    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/admin/create",
     *      summary="Create an admin user",
     *      tags={"Admin"},
     *      description="Create an admin user",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="User Details",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="email",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="password",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="last_name",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="first_name",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="password_confirmation",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="object",
     *                  @SWG\Schema(
     *                      type="object",
     *                      @SWG\Property(
     *                          property="token",
     *                          type="string"
     *                      )
     *                  )
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=422,
     *          description="invalid data",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )  
     * )
     */
    public function save(UserCreationRequest $request)
    {
        // create a normal user with the repository
        $user = $this->userRepository->asAdmin()->create($request->validated());

        // return created user
        return $this->sendResponse($user->toArray(), 'User Created SuccessFully');
    }


    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/admin/login",
     *      summary="Login as an admin",
     *      tags={"Admin"},
     *      description="Loging as an admin",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="User credentials",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="email",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="password",
     *                  type="string"
     *              ),
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="object",
     *                  @SWG\Schema(
     *                      type="object",
     *                      @SWG\Property(
     *                          property="token",
     *                          type="string"
     *                      )
     *                  )
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=422,
     *          description="invalid data",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=401,
     *          description="invalid credentials",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     *  
     * )
     */
    public function login(LoginRequest $request)
    {
        // $user = User::where('email', $request->input('email'))->get()->first();
        // if ($user && (!$user->is_admin || $user->is_admin == 0)) 
        //     return response()->json(['success' => false, 'message' => 'You need an admin account to login as admin'], 403); ;

        $credentials = $request->validated();
        $token = $this->userRepository->login($credentials);
        return $token ? $this->sendResponse(['token' => $token], 'Login SuccessFul') : $this->sendError('Invalid Credentials', 401);
    }


    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/admin/logout",
     *      summary="Logout as an admin",
     *      tags={"Admin"},
     *      description="Logout as an admin",
     *      produces={"application/json"},
     *      security={ {"api_key": {}} },
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function logout()
    {
        $success = $this->userRepository->logout();
        return $success ? $this->sendSuccess('Logout SuccessFul') : $this->sendError('Error Logging Out', 500);
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/admin/user-listing",
     *      summary="Get a listing of the non admin users",
     *      tags={"Admin"},
     *      description="Get all Non Admin Users",
     *      produces={"application/json"},
     *      security={ {"api_key": {}} },
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/User")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function user_listing(Request $request)
    {
        $users = $this->userAccountRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        )->where('role_id', 2);

        return $this->sendResponse($users->toArray(), 'User Listing Retrived SuccessFully');
    }


    /**
     * @param string $uuid
     * @return Response
     *
     * @SWG\Delete(
     *      path="/user-delete/{uuid}",
     *      summary="Remove the specified User account",
     *      tags={"Admin"},
     *      description="Remove the specified User account",
     *      produces={"application/json"},
     *      security={ {"api_key": {}} },
     *      @SWG\Parameter(
     *          name="uuid",
     *          description="uuid of User",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy_user_account($uuid)
    {
        $user = $this->userAccountRepository->findByUUID($uuid);

        if (empty($user)) {
            return $this->sendError('User Account not found', 404);
        }

        if ($user->isAdmin()) {
            return $this->sendError('Admin Account cannot be deleted', 403);
        }

        return $this->sendSuccess('User Account deleted successfully');
    }
}
