<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBrandAPIRequest;
use App\Http\Requests\API\UpdateBrandAPIRequest;
use App\Models\Brand;
use App\Repositories\BrandRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class BrandController
 * @package App\Http\Controllers\API
 */

class BrandAPIController extends AppBaseController
{
    /** @var  BrandRepository */
    private $brandRepository;

    public function __construct(BrandRepository $brandRepo)
    {
        $this->brandRepository = $brandRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/brands",
     *      summary="Get a listing of the Brands.",
     *      tags={"Brand"},
     *      description="Get all Brands",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Brand")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $brands = $this->brandRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($brands->toArray(), 'Brands retrieved successfully');
    }

    /**
     * @param CreateBrandAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/brand/create",
     *      summary="Store a newly created Brand in storage",
     *      tags={"Brand"},
     *      description="Store Brand",
     *      produces={"application/json"},
     *      security={ {"api_key": {}} },
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Brand that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Brand")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Brand"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateBrandAPIRequest $request)
    {
        $input = $request->all();

        $brand = $this->brandRepository->create($input);

        return $this->sendResponse($brand->toArray(), 'Brand saved successfully');
    }

    /**
     * @param string $uuid
     * @return Response
     *
     * @SWG\Get(
     *      path="/brand/{uuid}",
     *      summary="Display the specified Brand",
     *      tags={"Brand"},
     *      description="Get Brand",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="uuid",
     *          description="uuid of Brand",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Brand"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($uuid)
    {
        /** @var Brand $brand */
        $brand = $this->brandRepository->findByUUID($uuid);

        if (empty($brand)) {
            return $this->sendError('Brand not found');
        }

        return $this->sendResponse($brand->toArray(), 'Brand retrieved successfully');
    }

    /**
     * @param string $uuid
     * @param UpdateBrandAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/brand/{uuid}",
     *      summary="Update the specified Brand in storage",
     *      tags={"Brand"},
     *      description="Update Brand",
     *      produces={"application/json"},
     *      security={ {"api_key": {}} },
     *      @SWG\Parameter(
     *          name="uuid",
     *          description="uuid of Brand",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Brand that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Brand")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Brand"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($uuid, UpdateBrandAPIRequest $request)
    {
        $input = $request->all();

        /** @var Brand $brand */
        $brand = $this->brandRepository->findByUUID($uuid);

        if (empty($brand)) {
            return $this->sendError('Brand not found');
        }

        $brand = $this->brandRepository->update($input, $brand->id);

        return $this->sendResponse($brand->toArray(), 'Brand updated successfully');
    }

    /**
     * @param string $uuid
     * @return Response
     *
     * @SWG\Delete(
     *      path="/brand/{uuid}",
     *      summary="Remove the specified Brand from storage",
     *      tags={"Brand"},
     *      description="Delete Brand",
     *      produces={"application/json"},
     *      security={ {"api_key": {}} },
     *      @SWG\Parameter(
     *          name="uuid",
     *          description="uuid of Brand",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($uuid)
    {
        /** @var Brand $brand */
        $brand = $this->brandRepository->findByUUID($uuid);

        if (empty($brand)) {
            return $this->sendError('Brand not found');
        }

        $brand->delete();

        return $this->sendSuccess('Brand deleted successfully');
    }
}
