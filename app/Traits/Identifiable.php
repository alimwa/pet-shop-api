<?php

/**
 * A trait to autogenerate UUIDs for all models.
 * 
 */

namespace App\Traits;

use Illuminate\Support\Str;

trait Identifiable
{

    protected static function boot()
    {
        // Boot other traits on the Model
        parent::boot();

        /**
         * Listen for the creating event on the user model.
         * Sets the 'uuid' to a UUID using Str::uuid() on the instance being created
         */
        static::creating(function ($model) {
            
            if ($model->uuid === null) {
                $model->setAttribute('uuid', Str::uuid()->toString());
            }
        });
        

        /**
         * Listen for the saving event on the user model.
         * Sets the 'uuid' to a UUID using Str::uuid() on the instance being created
         */
        static::saving(function ($model) {
            
            if ($model->uuid === null) {
                $model->setAttribute('uuid', Str::uuid()->toString());
            }
        });
    }

    // checks if the model has a valid uuid generated for it.
    public function hasValidUUID()
    {
        return Str::isUuid($this->uuid);
    }
}
