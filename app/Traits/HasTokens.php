<?php

namespace App\Traits;

use DateTimeImmutable;

trait HasTokens
{
    private $token;

    private $config;

    private function getConfig()
    {
        $this->config = config('lcobucci.configuration');
        return $this->config;
    }

    public function createToken()
    {
        $now   = new DateTimeImmutable();
        
        $this->token = $this->getConfig()
            // Configures the issuer (iss claim)
            ->issuedBy(env('APP_URL'))
            // Configures the audience (aud claim)
            ->permittedFor(env('APP_URL'))
            // Configures the time that the token was issue (iat claim)
            ->issuedAt($now)
            // Configures the expiration time of the token (exp claim)
            ->expiresAt($now->modify('+24 hour'))
            // Configures a new claim, called "uid"
            ->withClaim('uuid', $this->uuid)
            // Builds a new token
            ->getToken($this->config->signer(), $this->config->signingKey());
    }
}
