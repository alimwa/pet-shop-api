<?php

namespace App\Traits;

trait HasKeys
{
    private $publicPath;
    private $privatePath;

    public function setPaths()
    {
        // set file paths for keys
        $this->publicPath = config('lcobucci.paths.lcobbuci_base')."".config('lcobucci.paths.public');
        $this->privatePath = config('lcobucci.paths.lcobbuci_base')."".config('lcobucci.paths.private');
    }   


    public function generate()
    {
        $this->setPaths();

        (new \Spatie\Crypto\Rsa\KeyPair())->generate($this->privatePath, $this->publicPath);
    }
}
