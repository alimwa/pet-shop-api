<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Traits\HasKeys;



class GenerateLCobucciKeys extends Command
{
    use HasKeys;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lcobucci_key:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates private and public keys for LCobbuci JWT Signatures';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {   
        $this->generate();
        echo "LCobucci Keys generated successfully you are all set to start issuing tokens.\n";
        return 0;
    }
}
