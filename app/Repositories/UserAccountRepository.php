<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\BaseRepository;

/**
 * Class UserAccountRepository
 * @package App\Repositories
 * @version February 20, 2022, 10:26 am UTC
*/

class UserAccountRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'uuid',
        'avatar',
        'first_name',
        'last_name',
        'email',
        'email_verified_at',
        'address',
        'phone_number',
        'last_login_at',
        'role_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }
}
