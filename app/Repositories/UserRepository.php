<?php

namespace App\Repositories;


use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserRepository
{
    private $attributes = ['role_id' => 2];

    /**
     * Sets the isAdmin attribute of the user details.
     */
    public function asAdmin()
    {
        $this->attributes['role_id'] = 1;
        return $this;
    }


    /**
     * Creates user based on attributes
     */
    public function create($details)
    {
        $this->attributes = array_merge($this->attributes, $details);

        $user = User::create($this->attributes);

        event(new Registered($user));

        return $user;
    }

    /**
     * Tries to authenticate a user based on credentials.
     */
    public function login($credentials)
    {
        $user = User::where('email', $credentials['email'])->get()->first();

        if(!$user) return null;

        if(Hash::check($credentials['password'], $user->password))
        {
            $user->tokens()->delete();

            $token =  $user->createToken('PET_SHOP_TOKEN');

            $user->last_login_at = Carbon::now();

            $user->save();

            return $token->plainTextToken;
        }    
 
        return null;
    }


    /**
     * ends a user session.
     */
    public function logout()
    {
        request()->user()->tokens()->delete();
        return ['success' => true, 'message' => 'Logout Successful'];        
    }

    /**
     * Gets all non admin users.
     */
    public function all_users()
    {
        return User::where('role_id', 2)->get();
    }


    /**
     * Edits user details.
     */
    public function edit($details)
    {
        $user = request()->user();
        foreach($details as $field => $value)
        {
            $user->{$field} = $value;
        }
        return $user->save();
    }


    /**
     * Deletes a user account
     */
    public function delete()
    {
        $user = request()->user();
        $user->tokens()->delete();
        return $user->delete();
    }


    /**
     * Gets the orders of the currently logged in user.
     */
    public function orders()
    {
        return [];
    }
}
