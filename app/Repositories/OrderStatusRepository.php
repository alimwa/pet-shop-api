<?php

namespace App\Repositories;

use App\Models\OrderStatus;
use App\Repositories\BaseRepository;

/**
 * Class OrderStatusRepository
 * @package App\Repositories
 * @version February 20, 2022, 12:05 am UTC
*/

class OrderStatusRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'uuid',
        'title'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return OrderStatus::class;
    }
}
