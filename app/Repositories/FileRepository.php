<?php

namespace App\Repositories;


use App\Models\File;
use Exception;

class FileRepository
{
    private $folder = 'pet-shop';

    /**
     * Saves a file on disk and returns the model instance with its information.
     */
    public function save()
    {
        try
        {
            $fileOnDisk = request()->file('file')->store($this->folder);
            $file = File::create([
                'name' => request()->file('file')->getBasename(),
                'path' => $fileOnDisk,
                'size' => request()->file('file')->getSize(),
                'type' => request()->file('file')->getMimeType()
            ]);
            return $file;
        }
        catch(Exception $e)
        {
            return null;
        }
        
    }


    public function getByUUID($uuid)
    {
        return File::where('uuid', $uuid)->get()->first();
    }
}
